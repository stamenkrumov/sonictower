﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class WallGenerator : MonoBehaviour {

	public static event Action<Vector3> spawnWallAtPositionEvent;

	public GameObject wallPrefab;

    public GameObject wallTileLeft;
    public GameObject wallTileRight;

    public int wallTilesCount = 10;

    public GameObject platform;

    private List<GameObject> tilesLeft;
    private List<GameObject> tilesRight;

	private bool isNextWallGenerated = false;

    // Use this for initialization
    void Start()
    {
        tilesLeft = new List<GameObject>();
        tilesRight = new List<GameObject>();

        for (int i = 0; i < wallTilesCount; i++)
        {


            GameObject newTileLeft = Instantiate(wallTileLeft) as GameObject;
            newTileLeft.transform.position = new Vector3(wallTileLeft.transform.position.x,
                                                     wallTileLeft.transform.position.y + wallTileLeft.GetComponent<SpriteRenderer>().bounds.size.y * (i + 1));

            newTileLeft.transform.parent = gameObject.transform;

            newTileLeft.tag = "Wall";

            tilesLeft.Add(newTileLeft);

            GameObject newTileRight = Instantiate(wallTileRight) as GameObject;
            newTileRight.transform.position = new Vector3(wallTileRight.transform.position.x,
                                                     wallTileRight.transform.position.y + wallTileRight.GetComponent<SpriteRenderer>().bounds.size.y * (i + 1));

            newTileRight.transform.parent = gameObject.transform;

            newTileRight.tag = "Wall";

            tilesRight.Add(newTileRight);

            if ((i % 3 == 0 && i>1) || (gameObject.name != "Wall" && i % 3 == 0) )
            {
                int tilesCount = UnityEngine.Random.Range(3, 10);

                float randX = UnityEngine.Random.Range(newTileLeft.transform.position.x,
                                           newTileRight.transform.position.x - tilesCount * wallTileRight.GetComponent<SpriteRenderer>().bounds.size.x);

                Vector3 platformPosition = new Vector3(randX, newTileRight.transform.position.y);

                GameObject newPlatform = Instantiate(platform);

                newPlatform.transform.parent = gameObject.transform;

                SonicGameManager.instance.platformsGenerated++;

                if ( SonicGameManager.instance.platformsGenerated % 100 == 0 )
                {
                    platformPosition = newTileLeft.transform.position;
                    tilesCount = 26;
                }
              
                newPlatform.GetComponent<PlatformGenerator>().GeneratePlatform(platformPosition, tilesCount);

            }

        }
    }

	void OnTriggerEnter2D(Collider2D collider)
	{
		if (collider.gameObject.tag == "Player" && !isNextWallGenerated) {

			Vector3 position = new Vector3(gameObject.transform.position.x,
			                                          gameObject.transform.position.y + 10.2f);

			print ("Wall Trigger");
			if( spawnWallAtPositionEvent != null )
			{
				spawnWallAtPositionEvent(position);
			}

			isNextWallGenerated = true;
		}
	}
}
