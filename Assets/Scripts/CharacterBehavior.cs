﻿using UnityEngine;
using System.Collections;
using System;

public class CharacterBehavior : MonoBehaviour {

	public static event Action gameOverEvent;

    public float movementSpeed = 10f;
    public float jumpForce = 50f;

    private bool isInJumpPhase = false;
    private bool isInRunningPhase = false;
    private Rigidbody2D characterRegidBody;


    private bool isTurnedLeft = false;

	private int previousPlatformNumber = 0;

    private Animator animator;

	// Use this for initialization
	void Start () {
        characterRegidBody = GetComponent<Rigidbody2D>();

        animator = GetComponent<Animator>();

    }
	
	// Update is called once per frame
	void Update () { 
        

        isInRunningPhase = false;

        if( Input.GetKeyDown(KeyCode.LeftArrow) || Input.GetKeyDown(KeyCode.RightArrow) )
        {
            characterRegidBody.velocity = new Vector2(0, characterRegidBody.velocity.y);
        }

        if (Input.GetKey(KeyCode.RightArrow))
        {
            if (isTurnedLeft)
            {
                gameObject.transform.localScale = new Vector3(gameObject.transform.localScale.x * (-1),
                                                              gameObject.transform.localScale.y);
                isTurnedLeft = false;
            }

            characterRegidBody.AddForce(Vector2.right * movementSpeed);
            isInRunningPhase = true;
        }

        if (Input.GetKey(KeyCode.LeftArrow))
        {
            if(!isTurnedLeft)
            {
                gameObject.transform.localScale = new Vector3(gameObject.transform.localScale.x * (-1),
                                                              gameObject.transform.localScale.y);

                isTurnedLeft = true;
            }

            characterRegidBody.AddForce(Vector2.left * movementSpeed);
            isInRunningPhase = true;
        }

        if (Input.GetKeyDown(KeyCode.Space) && !isInJumpPhase)
        {
            isInJumpPhase = true;
            var extraJump = 1f;
            if(isInRunningPhase)
            {
                extraJump = 1.5f;
            }
            characterRegidBody.AddForce(Vector2.up * jumpForce*extraJump);
        }

    }

    void FixedUpdate()
    {
        //print(Mathf.Abs(characterRegidBody.velocity.x));
        animator.SetFloat("Speed", Mathf.Abs(characterRegidBody.velocity.x));
        animator.SetBool("isJumping", isInJumpPhase);
    }

    void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Ground")
        {
            isInJumpPhase = false;

			var platformNumber = collision.gameObject.GetComponentInParent<PlatformGenerator>().GetNumberOfPlatform();

			print("Number"+platformNumber);

			if( platformNumber > previousPlatformNumber )
			{
				SonicGameManager.instance.platformsPassed++;
				previousPlatformNumber = platformNumber;
				print (SonicGameManager.instance.platformsPassed);
			}

        }
    }

	void OnBecameInvisible()
	{
		print ("GameOver");

		if( gameOverEvent != null )
		{
			gameOverEvent();
		}
	}
}
