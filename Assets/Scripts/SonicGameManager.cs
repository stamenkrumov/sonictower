﻿
using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class SonicGameManager : MonoBehaviour {

    public static SonicGameManager instance;

    public int platformsPassed = 0;
    public int platformsGenerated = 0;
	public int wallsGenerated = 0;
	public GameObject wallPrefab;

	public Text points;
	public Text levels;

	public GameObject gameOverPanel;
	public Text gameOverScore;
	public Text gameOverLevels;

    void Awake()
    {
        if( instance != null )
        {
            return;
        }

        instance = this;
    }

	// Use this for initialization
	void Start () {
	    WallGenerator.spawnWallAtPositionEvent += HandlespawnWallAtPositionEvent;
		CharacterBehavior.gameOverEvent += HandlegameOverEvent;
	}

	void HandlegameOverEvent ()
	{
		points.gameObject.transform.parent.gameObject.SetActive (false);
		gameOverPanel.SetActive (true);
		gameOverScore.text = (platformsPassed * 10).ToString ();
		gameOverLevels.text = platformsPassed.ToString ();
		Time.timeScale = 0;
	}

	void HandlespawnWallAtPositionEvent (Vector3 position)
	{
		GameObject newWall = Instantiate (wallPrefab) as GameObject;
		newWall.transform.position = position;
		newWall.name = "Wall" + wallsGenerated;
		wallsGenerated++;
		print ("WallsGenerated");
	}

	void Update()
	{
		if (points.gameObject.transform.parent.gameObject.activeSelf) {
			points.text = (platformsPassed * 10).ToString ();
			levels.text = platformsPassed.ToString ();
		}
	}
}
