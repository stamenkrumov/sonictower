﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class PlatformGenerator : MonoBehaviour {

    public GameObject platformTile;
    public Sprite endTileLeft;
    public Sprite endTileRight;
    public Sprite tileMiddle;

	public GameObject platformCountText;

    public bool isFirstPlatform = false;

	private int platformNumber = 0;

    private List<GameObject> tiles;
    public List<GameObject> Tiles
    {
        get { return tiles; }
    }

    // Use this for initialization
    void Awake()
    {
        tiles = new List<GameObject>();
    }

    void Start()
    {
        if( isFirstPlatform )
        {
            GeneratePlatform(gameObject.transform.position, 26);
        }
    }


    public void GeneratePlatform(Vector3 position, int tilesCount)
    {
		platformNumber = SonicGameManager.instance.platformsGenerated;

		if (isFirstPlatform) {
			platformNumber = 0;
		}

        gameObject.transform.position = position;

        GameObject newTile = Instantiate(platformTile) as GameObject;
        newTile.transform.position = new Vector3(platformTile.transform.position.x +
                                                 platformTile.GetComponent<SpriteRenderer>().bounds.size.x,
                                                 platformTile.transform.position.y);
        platformTile.GetComponent<SpriteRenderer>().sprite = endTileLeft;
        newTile.transform.parent = gameObject.transform;

        newTile.tag = "Ground";

        for (int i = 1; i < tilesCount - 1; i++)
        {
            newTile = Instantiate(platformTile) as GameObject;
            newTile.transform.position = new Vector3(platformTile.transform.position.x +
                                                    platformTile.GetComponent<SpriteRenderer>().bounds.size.x * (i + 1),
                                                    platformTile.transform.position.y);

            newTile.transform.parent = gameObject.transform;

            newTile.GetComponent<SpriteRenderer>().sprite = tileMiddle;

            tiles.Add(newTile);

            newTile.tag = "Ground";
        }

        newTile = Instantiate(platformTile) as GameObject;
        newTile.transform.position = new Vector3(platformTile.transform.position.x +
                                                 platformTile.GetComponent<SpriteRenderer>().bounds.size.x * tilesCount,
                                                 platformTile.transform.position.y);
        newTile.GetComponent<SpriteRenderer>().sprite = endTileRight;
        newTile.transform.parent = gameObject.transform;

        newTile.tag = "Ground";

		if (SonicGameManager.instance.platformsGenerated % 10 == 0 && SonicGameManager.instance.platformsGenerated != 0) {
			platformCountText.SetActive(true);
			platformCountText.transform.position = tiles[(int)(tiles.Count)/2].transform.position;
		}
    }

	public int GetNumberOfPlatform()
	{
		return platformNumber;
	}

}
