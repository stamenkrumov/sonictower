﻿using UnityEngine;
using System.Collections;

public class CameraController : MonoBehaviour {

    public GameObject mainCharacter;

	[HideInInspector]
	public int cameraVelocity = 500;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		gameObject.transform.position += Vector3.up / cameraVelocity; //new Vector3(transform.position.x, mainCharacter.transform.position.y+3.5f, -10)
	}
}
