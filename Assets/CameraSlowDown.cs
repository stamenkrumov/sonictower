﻿using UnityEngine;
using System.Collections;

public class CameraSlowDown : MonoBehaviour {

	public Camera mainCamera;

	void OnTriggerEnter2D(Collider2D collider)
	{
		if (collider.gameObject.tag == "Player") {

			mainCamera.GetComponent<CameraController>().cameraVelocity = 500;
		}
	}
}
