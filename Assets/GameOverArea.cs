﻿using UnityEngine;
using System.Collections;

public class GameOverArea : MonoBehaviour {

	void OnTriggerEnter2D(Collider2D collider)
	{
		if (collider.gameObject.tag == "Player") {
			print ("Disable collider");
			collider.enabled = false;
		}
	}
}
