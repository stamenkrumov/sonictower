﻿using UnityEngine;
using System.Collections;

public class WallDestroy : MonoBehaviour {

	void OnBecameInvisible()
	{
		Destroy (gameObject);
		Destroy (gameObject.transform.parent.gameObject);
	}
}
