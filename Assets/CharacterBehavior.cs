﻿using UnityEngine;
using System.Collections;

public class CharacterBehavior : MonoBehaviour {

    public float movementSpeed = 10f;
    public float jumpForce = 50f;

    private bool isInJumpPhase = false;
    private bool isInRunningPhase = false;
    private Rigidbody2D characterRegidBody;

    private Animator animator;

	// Use this for initialization
	void Start () {
        characterRegidBody = GetComponent<Rigidbody2D>();

        animator = GetComponent<Animator>();

    }
	
	// Update is called once per frame
	void Update () { 
        

        isInRunningPhase = false;

        if (Input.GetKey(KeyCode.RightArrow))
        {

            characterRegidBody.AddForce(Vector2.right * movementSpeed);
            isInRunningPhase = true;
        }

        if (Input.GetKey(KeyCode.LeftArrow))
        {
            characterRegidBody.AddForce(Vector2.left * movementSpeed);
            isInRunningPhase = true;
        }

        if (Input.GetKeyDown(KeyCode.Space) && !isInJumpPhase)
        {
            isInJumpPhase = true;
            var extraJump = 1f;
            if(isInRunningPhase)
            {
                extraJump = 1.3f;
            }
            characterRegidBody.AddForce(Vector2.up * jumpForce*extraJump);
        }

    }

    void FixedUpdate()
    {
        //print(Mathf.Abs(characterRegidBody.velocity.x));
        animator.SetFloat("Speed", Mathf.Abs(characterRegidBody.velocity.x));
    }

    void OnCollisionEnter2D(Collision2D collision)
    {
        print(collision.collider.tag);
        if (collision.collider.tag == "Ground")
        {
            print("Collision");
            isInJumpPhase = false;
        }

        if(collision.collider.tag == "Wall")
        {
            if(Input.GetKey(KeyCode.Space))
            {
                characterRegidBody.AddForce(Vector2.up * jumpForce);
                print("Wall");
            }
        }
    }
}
