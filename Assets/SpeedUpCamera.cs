﻿using UnityEngine;
using System.Collections;

public class SpeedUpCamera : MonoBehaviour {

	public Camera mainCamera;
	
	void OnTriggerEnter2D(Collider2D collider)
	{
		if (collider.gameObject.tag == "Player") {
			
			mainCamera.GetComponent<CameraController>().cameraVelocity = 5;
		}
	}
}
